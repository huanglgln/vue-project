import { createApp, h } from 'vue'
import App from './App.vue'

import router from './router'
import store from './store'
import vant from 'vant'
import 'vant/lib/index.css'
// import * as echarts from 'echarts';
// import { plugin } from 'echarts-for-vue';
import './mock' //TODO mock测试数据 上线删除此代码
// import.meta.env.VITE_MOCK === "true" && import('./mock') //TODO mock测试数据 上线删除此代码
const app = createApp(App)

app.use(router)
app.use(store)
app.use(vant)
// app.use(plugin, { echarts, h });                        // 作为插件使用
app.mount('#app')
