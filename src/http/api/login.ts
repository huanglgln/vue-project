import request from '../request'

/**
 * 登录接口
 * @param data axios参数 
 * @return promise对象
 */
export function loginHttp(data: any) {
    return request({
        url: '/api/login.do',
        method: 'POST',
        data: data
    }, {
        showLoading: true,
        cache: false
    });
}