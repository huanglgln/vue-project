import axios from 'axios';
import { Toast, Notify } from 'vant'
// import cacheTool from './cache.js' TODO

// let baseURL = process.env.apiUrl;
//  创建axios实例
const service = axios.create({
  baseURL: '/api', // api的base_url
  timeout: 100 * 1000, // 请求超时时间
});

service.interceptors.request.use(
  (config: any) => {
    //token验证
    // if (store.getters.token) {
    //   // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
    //   config.headers['X-Token'] = getToken()
    // }
    return config;
  },
  error => {
    //请求错误处理
    Promise.reject(error);
  },
);

//  response拦截器
service.interceptors.response.use(
  (response: any) => {
    //成功请求到数据
    const { status, data } = response
    const { code, msg } = data
    if (status === 200) {   // http状态码
      if (data.code === "00000000") {   // 后台返回code码
        return Promise.resolve(data.data)
      } else {
        Notify({ type: 'danger', message: data.msg });
        return false;
      }
    } else {
      Notify({ type: 'danger', message: '请求错误' });
    }
  },
  error => {
    Notify({ type: 'warning', message: '请求超时' });
    return Promise.reject(error);
  },
);

// 配置缓存请求api,用于缓存get和post请求数据,
// const cacheRequest = cacheTool(service, {
//   cacheTime: 5, //缓存周期 默认是5s 单位秒
//   MaxCacheLen: 1000 //最多缓存接口数 默认是1000条
// })


type configData = {
  showLoading?: boolean;
  cache?: boolean
}

/**
 * 
 * @param axiosParams axios参数
 * @param config config参数
 *        showLoading  boolean  请求是否显示loading;
 *        cache boolean | Number 缓存设置，true为启用默认缓存时间， Number为自定义缓存时间;
 * @returns Promise对象
 */
const requestHttp = function (axiosParams: any, config?: configData) {
  config = config ? config : {};
  const { showLoading, cache } = config;
  // let httpClient = cache ? cacheRequest : service // 判断缓存cache，为true时启用默认缓存时间，为Number时为自定义缓存时间单位秒
  let httpClient = service;
  let loading: any;
  if (showLoading) {
    loading = Toast.loading({
      message: '加载中...',
      forbidClick: true,
      duration: 0
    })
  }
  return new Promise<void>((resolve, reject) => {
    httpClient({ ...axiosParams, config }).then((res: any) => {
      resolve(res);
    }).catch((err: any) => {
      console.log(err);//TODO
    }).finally(() => {
      loading && loading.clear()
    })
  })
}

export default requestHttp;
