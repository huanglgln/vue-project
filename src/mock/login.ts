import Mock from 'mockjs'
const Random = Mock.Random
const userMap = {
  admin: {
    code: '00000000',
    msg: "成功！",
    data: {
      roles: ['admin'],
      token: 'admin',
      name: 'SuperAdmin',
      title: Random.csentence(5, 30), //  Random.csentence( min, max )
      imgSrc: Random.dataImage('200x160', '这是图片中的文本'), // Random.dataImage( size, text ) 生成图片（base64位数据格式）
      author_name: Random.cname(), // Random.cname() 随机生成中文名
      date: Random.date() + ' ' + Random.time() // Random.date()指示生成的日期字符串的格式,默认为yyyy-MM-dd；Random.time() 返回一个随机的时间字符串
    }
  }
}

export default {
  /**
   * 登录测试数据
   * @param config axis参数
   * @returns 测试数据
   */
  loginByUsername: (config: any): Object => {
    const { userName } = JSON.parse(config.body)
    return userMap[userName]
  }
}
