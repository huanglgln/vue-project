/// <reference types="vite/client" />

declare module '*.vue' {
  import { DefineComponent } from 'vue'
  // eslint-disable-next-line
  const component: DefineComponent<{}, {}, any>
  export default component
}

interface ImportMetaEnv {
  /**
   * URL地址
   */
  readonly VITE_BASE_URL: string
  /**
   * MOCK开关 loadEnv加载的evn配置都为string类型
   */
  readonly VITE_MOCK: string
  // 更多环境变量...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
