import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import pxtovw from 'postcss-px-to-viewport'

const loder_pxtovw = pxtovw({
  unitToConvert: "px", // 要转化的单位
  viewportWidth: 750, // UI设计稿的宽度
  unitPrecision: 6, // 转换后的精度，即小数点位数
  propList: ["*"], // 指定转换的css属性的单位，*代表全部css属性的单位都进行转换
  viewportUnit: "vw", // 指定需要转换成的视窗单位，默认vw
  fontViewportUnit: "vw", // 指定字体需要转换成的视窗单位，默认vw
  selectorBlackList: [], // 指定不转换为视窗单位的类名，
  minPixelValue: 1, // 默认值1，小于或等于1px则不进行转换
  mediaQuery: true, // 是否在媒体查询的css代码中也进行转换，默认false
  replace: true, // 是否转换后直接更换属性值
  exclude: [/node_modules/], // 设置忽略文件，用正则做目录名匹配
  landscape: false // 是否处理横屏情况
});

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  //检查process.cwd()路径下.env.${mode}.local、.env.${mode}、.env.local、.env这四个环境文件
  //https://www.kuxiaoxin.com/archives/40
  loadEnv(mode, process.cwd());
  //vite配置
  return {
    plugins: [vue()],
    resolve: {
      alias: {
        '@/': new URL('./src/', import.meta.url).pathname
      }
    },
    // 移动端适配px转vw
    css: {
      postcss: {
        plugins: [loder_pxtovw]
      }
    },
    server: {
      proxy: {
        '/api': {
          target: 'http://www.api.com',
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '')
        },
      }
    }
  }
})
